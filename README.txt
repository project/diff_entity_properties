INTRODUCTION
------------
The Diff Entity Properties module enables custom or pre-built entities to be
fully supported by the Diff module, creating a Revisions List page for all
entity types that have revisions enabled and allows permitted users view the
difference between two entity revisions, given that that entity type is
correctly configured.


MAINTAINERS
-----------
Current maintainers:
  * Felipe Caminada - https://www.drupal.org/u/caminadaf

This project has been sponsored by:
  * CI&T - CI&T has more experience than anyone else in leading Drupal
    implementations for multi-brand enterprises. When you need to provide better
    more timely, and relevant web experiences for your customers, CI&T takes
    you where you need to be. (http://drupal.org/node/1530378)


REQUIREMENTS
------------
This module depends on the "diff" module. (At least version 3.3)


INSTALLATION
------------
No special installation needed.


CONFIGURATION
------------
This module defines two permissions for each entity type following:

- view <entity_type> revisions list
- view <entity_type> revisions diff
These permissions requires the given entity_type to have Revisions enabled.

Then when using a user with the permissions above, you will be able to access
"/entity/<entity_type>/<id>/revisions/" which displays the list of revisions
for a given entity type and id.

Furthermore, if the entity is configured to, you may select two revisions and
compare them, similar to what Diff does.

This diff will compare the values for all properties of that entity type, but
the format may not be adequate, or there may be some properties that you do not
wish to compare. In this case, this module enables certain configurations:

First, to disable a certain property from showing on the diff, you need to
insert "skip diff" into that property's information (through property_info or
property_info_alter), for example:
```
function hook_get_entity_properties() {
  $properties = array();
  $properties['vid'] = array(
    'label' => t('Revision ID'),
    'type' => 'integer',
    'description' => t('The entity revision ID'),
    'schema field' => 'vid',
    'setter callback' => 'entity_property_verbatim_set',
    'skip diff' => TRUE,
  );
}
```

To change the way a property is rendered in the diff, you can implement
hook_entity_diff_options() and returning the list of properties that you wish to
alter. Then, for each property you need to implement a
hook_entity_diff_additional_options_<property_name>() returning the formatted
properties values in a structure similar to the example below:

```
/**
 * Helper function to render date properties on the diff page.
 */
function hook_entity_diff_additional_options_date_field($old_date, $new_date,
  $context) {
  $row = array();
  foreach ($context['states'] as $state) {
    $row['#states'][$state] = array(
      '#old' => array(format_date($old_date)),
      '#new' => array(format_date($new_date)),
    );
  }
  return $row;
}
```
